const path = require("path");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  ////////////////-------------CREATE MESSAGE TEMPLATE------------------////////////////
  // Query data for student messages
  const studentResult = await graphql(`
    query StudentMessages {
      allNodeMessage(filter: { path: { alias: { eq: "/message-student" } } }) {
        nodes {
          id
          path {
            alias
          }
        }
      }
    }
  `);

  // Create pages for each student message
  studentResult.data.allNodeMessage.nodes.forEach((node) => {
    createPage({
      path: node.path.alias,
      component: path.resolve("./src/components/HomePage/MessageStudent.js"),
      context: {
        id: node.id,
      },
    });
  });

  // Query data for staff messages
  const staffResult = await graphql(`
    query StaffMessages {
      allNodeMessage(filter: { path: { alias: { eq: "/message-staff" } } }) {
        nodes {
          id
          path {
            alias
          }
        }
      }
    }
  `);

  // Create pages for each staff message
  staffResult.data.allNodeMessage.nodes.forEach((node) => {
    createPage({
      path: node.path.alias,
      component: path.resolve("./src/components/HomePage/MessageStaff.js"),
      context: {
        id: node.id,
      },
    });
  });

  ///////////////---------DEPARTMENT TEMPLATE----------//////////////////
  const departmentResult = await graphql(`
    query {
      allNodeDepartmentoverview {
        nodes {
          path {
            alias
          }
        }
      }
    }
  `);

  // Create pages for each department
  departmentResult.data.allNodeDepartmentoverview.nodes.forEach(
    (department) => {
      createPage({
        path: department.path.alias,
        component: require.resolve("./src/templates/departmentTemplate.js"),
        context: {
          alias: department.path.alias,
        },
      });
    }
  );

  /////////////-----DepartmentDetail-----------//////////
  const departmentDetailResult = await graphql(`
    query {
      allNodeDepartmentDetail {
        nodes {
          id
          path {
            alias
          }
        }
      }
    }
  `);

  departmentDetailResult.data.allNodeDepartmentDetail.nodes.forEach(
    (department) => {
      createPage({
        path: department.path.alias,
        component: require.resolve(
          "./src/templates/departmentDetailTemplate.js"
        ),
        context: {
          id: department.id,
        },
      });
    }
  );

  ////////////---------GALLERY----------//////////////
  const galleryResult = await graphql(`
    query {
      allNodePhotoGallery {
        nodes {
          id
          path {
            alias
          }
        }
      }
    }
  `);

  const galleryNodes = galleryResult.data.allNodePhotoGallery.nodes;

  galleryNodes.forEach((node) => {
    createPage({
      path: node.path.alias,
      component: require.resolve("./src/templates/galleryTemplate.js"),
      context: {
        id: node.id,
      },
    });
  });
};
