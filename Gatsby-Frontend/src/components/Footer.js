import { graphql, useStaticQuery } from "gatsby";
import React from "react";

function Footer() {
  const data = useStaticQuery(graphql`
    query footer {
      allNodeFooter {
        nodes {
          path {
            alias
          }
          body {
            processed
          }
        }
      }
    }
  `);
  const footerNodes = data.allNodeFooter.nodes;

  return (
    <div className="bg-black text-center text-white bottom-0 p-4">
      {footerNodes.map((node, index) => (
        <div key={index}>
          <div dangerouslySetInnerHTML={{ __html: node.body.processed }} />
        </div>
      ))}
    </div>
  );
}

export default Footer;
