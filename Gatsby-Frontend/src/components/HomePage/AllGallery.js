import React from "react";
import { useState } from "react";
import { useStaticQuery, graphql } from "gatsby"; // Import necessary GraphQL dependencies

//Plugin
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import PhotoAlbum from "react-photo-album";
import Lightbox from "yet-another-react-lightbox";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Counter from "yet-another-react-lightbox/plugins/counter";

// CSS
import "yet-another-react-lightbox/plugins/counter.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";

function AllGallery() {
  // Query data using GraphQL
  const data = useStaticQuery(graphql`
    query {
      allNodeOnlyImage {
        nodes {
          path {
            alias
          }
          relationships {
            field_all_images {
              localFile {
                childImageSharp {
                  gatsbyImageData
                }
              }
            }
          }
          field_all_images {
            alt
          }
          body {
            processed
          }
        }
      }
    }
  `);

  const [index, setIndex] = useState(-1);

  const images = data.allNodeOnlyImage.nodes.flatMap((node) =>
    node.relationships.field_all_images.map((imageData) => {
      const imageSharp = imageData.localFile.childImageSharp; // Access childImageSharp directly
      return {
        src: imageSharp.gatsbyImageData.images.fallback.src,
        // alt: imageData.field_all_images.alt, // Use the correct field name
        width: imageSharp.gatsbyImageData.width,
        height: imageSharp.gatsbyImageData.height,
      };
    })
  );

  console.log("AllGallery=>", images);

  return (
    <div className="p-10">
      <p
        className="text-center mb-10"
        dangerouslySetInnerHTML={{
          __html: data.allNodeOnlyImage.nodes[0].body.processed,
        }}
      />
      <PhotoAlbum
        layout="rows"
        photos={images}
        targetRowHeight={150}
        onClick={({ index: current }) => setIndex(current)}
      />

      <Lightbox
        index={index}
        slides={images}
        plugins={[Thumbnails, Zoom, Fullscreen, Counter]}
        open={index >= 0}
        close={() => setIndex(-1)}
        styles={{ container: { backgroundColor: "rgba(0, 0, 0, .9)" } }}
      />
      <style>
        {`img.react-photo-album--photo:hover{
            transform: scale(1.05);
            transform: rotate(350deg);
            box-shadow: 0 0 10px 2px rgba(0,0,0,0.5);
            opacity: 0.8;
          }
        `}
      </style>
    </div>
  );
}

export default AllGallery;
