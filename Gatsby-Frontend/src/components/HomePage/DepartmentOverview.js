import React from "react";
import { useStaticQuery, graphql, Link } from "gatsby";

function DepartmentOverview() {
  const data = useStaticQuery(graphql`
    query DepartmentOverview {
      allNodeDepartmentoverview(
        filter: {
          path: { alias: { in: ["/arts-department", "/science-department"] } }
        }
      ) {
        nodes {
          title
          path {
            alias
          }
          body {
            processed
          }
        }
      }
      allNodeDepartmentDetail(
        filter: {
          path: { alias: { in: ["/commerce-department", "/bca-department"] } }
        }
      ) {
        nodes {
          path {
            alias
          }
          title
          id
          field_department_detail_text {
            processed
          }
        }
      }
    }
  `);

  const { allNodeDepartmentoverview, allNodeDepartmentDetail } = data;

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 gap-x-8 gap-y-4 bg-white p-12">
      {/* Column 1 and Column 2 */}
      {allNodeDepartmentoverview.nodes.map((node, index) => (
        <div
          key={index}
          className="col-span-1 shadow-xl shadow-blue-500/40 text-center p-8"
        >
          <Link to={node.path.alias}>
            <h1 className="text-lg font-bold pb-4">{node.title}</h1>
          </Link>
          <ol className="text-left">
            <li
              className="flex flex-row"
              dangerouslySetInnerHTML={{
                __html: node.body.processed,
              }}
            />
          </ol>
        </div>
      ))}

      {/* Column 3 and Column 4 */}
      {allNodeDepartmentDetail.nodes.map((detailNode, index) => (
        <div
          key={index}
          className="col-span-1 shadow-xl shadow-blue-500/40 text-center p-8"
        >
          <Link to={detailNode.path.alias}>
            <h2 className="text-lg font-bold text-center pb-4">
              {detailNode.title}
            </h2>
          </Link>
          <div
            className="text-justify"
            dangerouslySetInnerHTML={{
              __html: detailNode.field_department_detail_text.processed,
            }}
          />
        </div>
      ))}
    </div>
  );
}

export default DepartmentOverview;
