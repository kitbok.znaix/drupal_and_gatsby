import React from "react";
import { useStaticQuery, graphql } from "gatsby";
// import { GatsbyImage, getImage } from "gatsby-plugin-image";

function HeroImage() {
  // Use the `useStaticQuery` hook to fetch data
  const data = useStaticQuery(graphql`
    query HeroImage {
      allNodeHeroImage {
        nodes {
          field_hero_description {
            processed
          }
          relationships {
            field_hero_image_label {
              localFile {
                childrenImageSharp {
                  gatsbyImageData(layout: FULL_WIDTH)
                }
              }
            }
          }
        }
      }
    }
  `);

  const heroNodes = data.allNodeHeroImage.nodes;
  const { field_hero_description, relationships } = heroNodes[0];
  const image =
    relationships.field_hero_image_label.localFile.childrenImageSharp[0]
      .gatsbyImageData;

  return (
    <div
      className="min-h-screen flex justify-center items-center text-center bg-center bg-no-repeat bg-cover bg-fixed"
      style={{
        backgroundImage: `url(${image.images.fallback.src})`,
      }}
    >
      {/* <div className="absolute h-full inset-0 bg-gradient-to-t from-black via-transparent" /> */}
      <p
        className="text-4xl text-white mx-auto sm:mx-[10%] md:mx-[20%] mt-4 sm:mt-8 md:mt-[20%]"
        dangerouslySetInnerHTML={{
          __html: field_hero_description.processed,
        }}
      />
      {/* Add any other content or elements you want to display on top of the image */}
    </div>
  );
}

export default HeroImage;
