import * as React from "react";

// Plugin CSS
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/captions.css";

// PLUGIN
import Lightbox from "yet-another-react-lightbox";
import Inline from "yet-another-react-lightbox/plugins/inline";
import Captions from "yet-another-react-lightbox/plugins/captions";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";

import { graphql, useStaticQuery } from "gatsby"; // Import necessary Gatsby components

export default function HomeGallery() {
  // const [open, setOpen] = useState(false);
  const index = 0;
  const autoplay = true;
  const delay = 6000;

  // const toggleOpen = (state) => () => setOpen(state);

  // const updateIndex = ({ index: current }) => setIndex(current);

  const data = useStaticQuery(graphql`
    query {
      allNodePhotoGallery(
        filter: { path: { alias: { eq: "/home-page-gallery" } } }
      ) {
        nodes {
          relationships {
            field_photo_image {
              relationships {
                field_photo_image {
                  localFile {
                    childImageSharp {
                      gatsbyImageData(layout: CONSTRAINED, width: 3840)
                    }
                  }
                }
              }
              id
              field_photo_about {
                processed
              }
              field_photo_name {
                processed
              }
            }
          }
        }
      }
    }
  `);

  const descriptionTextAlign = "center";
  const descriptionMaxLines = 3;

  const images = data.allNodePhotoGallery.nodes.flatMap((node) =>
    node.relationships.field_photo_image.map((imageData) => {
      const imageSharp =
        imageData.relationships.field_photo_image.localFile.childImageSharp;
      return {
        id: imageData.id,
        src: imageSharp.gatsbyImageData.images.fallback.src,
        alt: imageData.field_photo_name.alt,
        width: imageSharp.gatsbyImageData.width,
        height: imageSharp.gatsbyImageData.height,
        about: imageData.field_photo_about.processed,
        name: imageData.field_photo_name.processed,
      };
    })
  );

  console.log(images);

  return (
    <div>
      <Lightbox
        index={index}
        plugins={[Inline, Slideshow, Captions]}
        captions={{ descriptionTextAlign, descriptionMaxLines }}
        slides={images.map((image) => ({
          src: image.src,
          alt: image.alt,
          width: image.width,
          height: image.height,
          title: image.name,
          description: (
            <div dangerouslySetInnerHTML={{ __html: image.about }} />
          ), // Corrected usage
        }))}
        slideshow={{ autoplay, delay }}
        // on={{
        //   view: updateIndex,
        //   click: toggleOpen(true),
        // }}
        carousel={{
          padding: 0,
          spacing: 0,
          imageFit: "cover",
        }}
        inline={{
          style: {
            width: "100%",
            maxWidth: "100%",
            height: "650px",
            aspectRatio: "3 / 2",
            margin: "0 auto",
            color: "white",
          },
        }}
        styles={{
          captionsDescriptionContainer: { padding: "5%" },
          captionsTitleContainer: { display: "none" },
          button: { display: "none" },
        }}
      />

      {/* <Lightbox
        open={open}
        close={toggleOpen(false)}
        index={index}
        slides={slides}
        on={{ view: updateIndex }}
        animation={{ fade: 0 }}
        controller={{ closeOnPullDown: true, closeOnBackdropClick: true }}
      /> */}

      <div class="absolute left-6 top-36 text-white sm:top-48 md:top-40 lg:top-48 sm:left-20 md:left-60 lg:left-80">
        <h1 class="mb-2 text-2xl font-extrabold sm:text-3xl md:text-4xl lg:text-5xl">
          KiT College:
        </h1>
        <p class="text-xs sm:text-sm md:text-base lg:text-lg">
          Start your journey now
        </p>
      </div>
    </div>
  );
}
