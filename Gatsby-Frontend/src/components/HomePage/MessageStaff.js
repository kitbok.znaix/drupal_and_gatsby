import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import MessageTemplate from "../../templates/messageTemplate";

const MessageStaff = () => {
  const data = useStaticQuery(graphql`
    query StaffMessage {
      allNodeMessage(filter: { path: { alias: { eq: "/message-staff" } } }) {
        nodes {
          title
          relationships {
            field_message_info {
              relationships {
                field_photo_image {
                  localFile {
                    childImageSharp {
                      gatsbyImageData(width: 200, height: 200)
                    }
                  }
                }
              }
              field_photo_name {
                processed
              }
              field_photo_about {
                processed
              }
            }
          }
        }
      }
    }
  `);

  const messages = data.allNodeMessage.nodes;

  return <MessageTemplate messages={messages} />;
};

export default MessageStaff;
