import React, { useEffect, useState } from "react";
import PhotoAlbum from "react-photo-album";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import Download from "yet-another-react-lightbox/plugins/download";
import Share from "yet-another-react-lightbox/plugins/share";
import Counter from "yet-another-react-lightbox/plugins/counter";
import "yet-another-react-lightbox/plugins/counter.css";
import axios from "axios";

function TestGallery() {
  const [index, setIndex] = useState(-1);
  const [gallery, setGallery] = useState([]);
  const kitPort =
    "http://192.168.101.17/ShillongPremiereLeague/read/R_Gallery.php";
  const baseUrl = "http://192.168.101.17/ShillongPremiereLeague/Read/";

  useEffect(() => {
    axios
      .get(kitPort)
      .then((res) => {
        setGallery(res.data.data); // Extract 'data' array from the response
        console.log(res.data.data); // Log the data received from the API
      })
      .catch((err) => console.log(err));
  }, []);

  const slides = gallery.map((photoData) => ({
    src: `${baseUrl}${photoData.photo}`, // Construct the image URL using baseUrl
    title: `Image ${photoData.id}`,
  }));

  console.log("testGallery=>", slides);

  return (
    <div className="p-10">
      <PhotoAlbum
        layout="rows"
        photos={slides}
        targetRowHeight={150}
        onClick={({ index: current }) => setIndex(current)}
        styles={{ container: { width: "100%" } }}
      />

      <Lightbox
        index={index}
        slides={slides}
        plugins={[Zoom, Fullscreen, Thumbnails, Download, Share, Counter]}
        open={index >= 0}
        close={() => setIndex(-1)}
        styles={{ container: { backgroundColor: "rgba(0, 0, 0, .9)" } }}
      />

      {/* Optional: Add any custom CSS styles here */}
    </div>
  );
}

export default TestGallery;
