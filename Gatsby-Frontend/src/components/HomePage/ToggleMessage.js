import React, { useState } from "react";
import MessageStaff from "./MessageStaff";
import MessageStudent from "./MessageStudent";

export default function ToggleMessage() {
  const [isStaff, setIsStaff] = useState(true);

  const toggleStaff = () => {
    setIsStaff(true);
  };

  const toggleStudent = () => {
    setIsStaff(false);
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen pb-0">
      <p className="text-xl font-semibold mb-4">MESSAGE</p>
      <div>
        <button
          onClick={toggleStaff}
          className={`mr-2 px-4 py-2 rounded ${
            isStaff ? "bg-blue-500 text-white" : "bg-gray-300 text-gray-700"
          }`}
        >
          Staff
        </button>
        <button
          onClick={toggleStudent}
          className={`px-4 py-2 rounded ${
            !isStaff ? "bg-blue-500 text-white" : "bg-gray-300 text-gray-700"
          }`}
        >
          Student
        </button>
      </div>
      {isStaff ? <MessageStaff /> : <MessageStudent />}
    </div>
  );
}
