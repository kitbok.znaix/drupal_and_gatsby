import React from "react";
import NavBar from "./NavBar";
import "../styles/global.css";

export default function Layout({ children }) {
  return (
    <div>
      <NavBar />
      <div className="pt-12">
        {/* Content for Each Page children from pages/index.js <Layout>all here are children</Layout> */}
        {children}
      </div>
    </div>
  );
}
