import React from "react";
import { Link } from "gatsby";

export default function Navbar() {
  return (
    <nav className="flex fixed z-50 justify-between w-full bg-blue-900">
      <h1 className="p-4 text-2xl font-bold text-white">KiT College</h1>
      <div className="flex p-4 space-x-4">
        <Link
          to="/"
          className="px-2 py-1 text-white rounded hover:text-blue-300"
          activeClassName="bg-blue-800"
        >
          Home
        </Link>
        <Link
          to="/about"
          className="px-2 py-1 text-white rounded hover:text-blue-300"
          activeClassName="bg-blue-800"
        >
          About Us
        </Link>
      </div>
    </nav>
  );
}
