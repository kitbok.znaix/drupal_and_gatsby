const breakpoints = [4320, 2160, 1080, 640, 384, 256, 128];

const unsplashLink = (id, width, height) =>
  `https://source.unsplash.com/${id}/${width}x${height}`;

const unsplashPhotos = [
  { id: "ts1zXzsD7xc", width: 1080, height: 1620 },
  { id: "F_r83HEzsXI", width: 1080, height: 1426 },
  { id: "m82uh_vamhg", width: 1080, height: 1440 },
  { id: "br-Xdb9KE0Q", width: 1080, height: 716 },
  { id: "6mze64HRU2Q", width: 1080, height: 1620 },
  { id: "7ENqG6Gmch0", width: 1080, height: 718 },
  // ... (remaining unsplashPhoto objects)
];

const slides = unsplashPhotos.map((photo) => {
  const width = photo.width * 4;
  const height = photo.height * 4;
  return {
    src: unsplashLink(photo.id, width, height),
    width,
    height,
    srcSet: breakpoints.map((breakpoint) => {
      const breakpointHeight = Math.round((height / width) * breakpoint);
      return {
        src: unsplashLink(photo.id, breakpoint, breakpointHeight),
        width: breakpoint,
        height: breakpointHeight,
      };
    }),
  };
});

const advancedSlides = [
  { ...slides[0], title: "Puppy in sunglasses", description: "Mollie Sivaram" },
  { ...slides[1], title: "Miami Beach", description: "Clark Van Der Beken" },
  { ...slides[2], title: "Flamingo", description: "Vicko Mozara" },
  // ... (remaining advancedSlides objects)
];

const slidesExport = [
  ...advancedSlides,
  {
    type: "video",
    title: "Big Buck Bunny",
    description: "The Peach Open Movie Project",
    width: 1280,
    height: 720,
    poster:
      "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg",
    sources: [
      {
        src: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        type: "video/mp4",
      },
    ],
  },
  // ... (remaining advancedSlides objects)
];

export default slidesExport;
