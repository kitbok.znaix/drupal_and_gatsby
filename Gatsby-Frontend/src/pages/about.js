import { graphql } from "gatsby";
import React from "react";
import Layout from "../components/Layout";

export default function About({ data }) {
  console.log(data);
  const { title, body } = data.allNodePage.nodes[0]; // Assuming there's only one node for /about-us

  return (
    <Layout>
      <div className="mx-auto max-w-[80%] p-8 bg-opacity-50 shadow-lg rounded-lg">
        <h1 className="text-3xl font-bold mb-4 text-center">{title}</h1>
        <div
          className="prose lg:prose-lg"
          dangerouslySetInnerHTML={{ __html: body[0].processed }}
        />
      </div>
    </Layout>
  );
}

export const query = graphql`
  query AboutUs {
    allNodePage(filter: { path: { alias: { eq: "/about-us" } } }) {
      nodes {
        title
        id
        path {
          alias
        }
        body {
          processed
        }
      }
    }
  }
`;
