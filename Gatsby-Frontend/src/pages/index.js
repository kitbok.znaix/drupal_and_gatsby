import React from "react";
import Layout from "../components/Layout";
import HomeGallery from "../components/HomePage/HomeGallery";
// import HeroImage from "../components/HomePage/HeroImage";
import DepartmentOverview from "../components/HomePage/DepartmentOverview";
// import MessageStudent from "../components/HomePage/MessageStudent";
// import MessageStaff from "../components/HomePage/MessageStaff";
import AllGallery from "../components/HomePage/AllGallery";
import ToggleMessage from "../components/HomePage/ToggleMessage";
import Footer from "../components/Footer";

import TestGallery from "../components/HomePage/TestPhoto";

function Home() {
  return (
    <Layout>
      <HomeGallery />
      {/* <HeroImage /> */}
      {/* <MessageStaff /> */}
      <DepartmentOverview />
      <ToggleMessage />
      {/* <MessageStudent /> */}
      <AllGallery />
      <TestGallery />
      <Footer />
    </Layout>
  );
}

export default Home;
