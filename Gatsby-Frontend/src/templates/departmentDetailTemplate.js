import React from "react";
import { graphql } from "gatsby";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import Layout from "../components/Layout";

function DepartmentDetailTemplate({ data }) {
  const departments = data.allNodeDepartmentDetail.nodes;
  console.log(data);

  return (
    <Layout>
      {departments.map((department) => (
        <div
          key={department.id}
          className="mx-auto max-w-[80%] p-8 bg-opacity-50 shadow-lg rounded-lg text-center"
        >
          <h1 className="text-lg font-bold pb-4">{department.title}</h1>
          <p
            dangerouslySetInnerHTML={{
              __html: department.field_department_detail_text.processed,
            }}
          />
          <div className="container shadow-xl shadow-red-500/40 p-8">
            {department.relationships.field_staff_detail.map((staff) => (
              <div className="py-8" key={staff.field_photo_name.processed}>
                <GatsbyImage
                  image={getImage(
                    staff.relationships.field_photo_image.localFile
                  )}
                  alt={staff.field_photo_image.alt}
                />
                <h2 className="italic">{staff.field_photo_name.processed}</h2>
                <p
                  dangerouslySetInnerHTML={{
                    __html: staff.field_photo_about.processed,
                  }}
                />
              </div>
            ))}
          </div>
        </div>
      ))}
    </Layout>
  );
}

export default DepartmentDetailTemplate;

export const query = graphql`
  query ($id: String!) {
    allNodeDepartmentDetail(filter: { id: { eq: $id } }) {
      nodes {
        title
        field_department_detail_text {
          processed
        }
        relationships {
          field_staff_detail {
            field_photo_name {
              processed
            }
            field_photo_about {
              processed
            }
            relationships {
              field_photo_image {
                localFile {
                  childImageSharp {
                    gatsbyImageData(height: 100, width: 100)
                  }
                }
              }
            }
            field_photo_image {
              alt
            }
          }
        }
      }
    }
  }
`;
