import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";

function DepartmentTemplate({ data }) {
  const department = data.nodeDepartmentoverview;
  const departmentTaxonomy = data.allTaxonomyTermDepartmentTax.nodes.find(
    (taxonomyNode) => taxonomyNode.path.alias === department.path.alias + "-tax"
  );

  return (
    <Layout>
      <div className="mx-auto max-w-[80%] p-8 bg-opacity-50 shadow-lg rounded-lg text-center">
        <h1 className="text-2xl font-bold mb-4">{department.title}</h1>
        <div
          className="prose"
          dangerouslySetInnerHTML={{ __html: department.body.processed }}
        />

        {departmentTaxonomy && (
          <div className="mt-8 text-left pl-[40%]">
            <ol>
              {departmentTaxonomy.relationships.node__department_detail.map(
                (detail, index) => (
                  <li className="pb-4" key={detail.id}>
                    <span className="pr-8">{index + 1}.</span>
                    <a href={detail.path.alias}>{detail.title}</a>
                  </li>
                )
              )}
            </ol>
          </div>
        )}
      </div>
    </Layout>
  );
}

export const query = graphql`
  query DepartmentDetails($alias: String!) {
    nodeDepartmentoverview(path: { alias: { eq: $alias } }) {
      path {
        alias
      }
      body {
        processed
      }
      title
    }
    allTaxonomyTermDepartmentTax {
      nodes {
        path {
          alias
        }
        relationships {
          node__department_detail {
            id
            title
            path {
              alias
            }
          }
        }
      }
    }
  }
`;

export default DepartmentTemplate;
