import React, { useEffect, useState } from "react";
import { graphql } from "gatsby";
import { GatsbyImage, getImage } from "gatsby-plugin-image";
import Layout from "../components/Layout";
import { FcNext, FcPrevious } from "react-icons/fc";

function GalleryTemplate({ data }) {
  const galleryNodes = data.allNodePhotoGallery.nodes;
  const slide = [];

  galleryNodes.forEach((galleryNode) => {
    galleryNode.relationships.field_photo_image.forEach((image) => {
      const imageData = getImage(
        image.relationships.field_photo_image.localFile
      );

      const slideItem = {
        field_photo_name: image.field_photo_name.processed,
        gatsbyImageData: imageData,
        field_photo_about: image.field_photo_about.processed,
      };

      slide.push(slideItem);
    });
  });

  const [currentImage, setCurrentImage] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentImage((prevImage) => (prevImage + 1) % slide.length);
    }, 5000); // 3000 milliseconds = 3 seconds

    return () => clearInterval(interval);
  }, [slide.length]);

  const prevSlide = () => {
    if (currentImage === 0) {
      setCurrentImage(slide.length - 1);
    } else {
      setCurrentImage(currentImage - 1);
    }
  };

  const nextSlide = () => {
    if (currentImage === slide.length - 1) {
      setCurrentImage(0);
    } else {
      setCurrentImage(currentImage + 1);
    }
  };

  console.log(slide[0]);

  return (
    <Layout>
      <div>
        {galleryNodes.map((galleryNode, index) => (
          <div key={index}>
            <h1 className="pt-4 pb-2 text-center text-lg">
              {galleryNode.title}
            </h1>
            <div className="h-[600px] w-[1000px] m-auto px-4">
              <h2 className="absolute top-[19%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-2xl z-10">
                {slide[currentImage].field_photo_name}
              </h2>
              <GatsbyImage
                image={slide[currentImage].gatsbyImageData}
                alt={slide[currentImage].field_photo_name}
                className="w-full h-full rounded-2xl duration-500 object-center object-cover"
              />
              <div
                className="bg-white opacity-70 w-[60%] absolute bottom-[5%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-lg z-10 p-6"
                dangerouslySetInnerHTML={{
                  __html: slide[currentImage].field_photo_about,
                }}
              />
            </div>
          </div>
        ))}
        <div>
          <FcNext
            onClick={nextSlide}
            className="absolute top-[50%] -translate-x-0 translate-y-[-50%] right-52 text-2xl rounded-full"
            size={30}
          />
          <FcPrevious
            onClick={prevSlide}
            className="absolute top-[50%] -translate-x-0 translate-y-[-50%] left-52 text-2xl rounded-full"
            size={30}
          />
        </div>
      </div>
    </Layout>
  );
}

export default GalleryTemplate;

export const query = graphql`
  query ($id: String!) {
    allNodePhotoGallery(filter: { id: { eq: $id } }) {
      nodes {
        title
        relationships {
          field_photo_image {
            field_photo_name {
              processed
            }
            relationships {
              field_photo_image {
                localFile {
                  childImageSharp {
                    gatsbyImageData
                  }
                }
              }
            }
            field_photo_about {
              processed
            }
            id
          }
        }
      }
    }
  }
`;
