import React from "react";
import { GatsbyImage, getImage } from "gatsby-plugin-image";

const MessageTemplate = ({ messages }) => {
  return (
    <div className="px-4">
      {messages.map((message, index) => (
        <div
          key={index}
          className="shadow-xl shadow-green-500/40 text-center p-8"
        >
          {/* <h1 className="text-lg font-bold pb-4">{message.title}</h1> */}
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 gap-x-8 gap-y-4">
            {message.relationships.field_message_info.map((info, infoIndex) => {
              const imageData = getImage(
                info?.relationships?.field_photo_image?.localFile
              );
              const imageName = info?.field_photo_name?.processed;
              const imageAbout = info?.field_photo_about?.processed;

              return (
                <div
                  key={infoIndex}
                  className="shadow-xl shadow-red-500/40 text-center p-8"
                >
                  <div>
                    {imageData && (
                      <GatsbyImage image={imageData} alt={imageName} />
                    )}
                  </div>
                  <div>
                    {imageName && <p className="text-sm">{imageName}</p>}
                  </div>
                  {imageAbout && (
                    <div>
                      <p
                        className="text-sm"
                        dangerouslySetInnerHTML={{ __html: imageAbout }}
                      />
                    </div>
                  )}
                </div>
              );
            })}
          </div>
        </div>
      ))}
    </div>
  );
};

export default MessageTemplate;
